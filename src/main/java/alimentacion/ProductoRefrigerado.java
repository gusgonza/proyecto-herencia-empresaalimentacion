package alimentacion;

public class ProductoRefrigerado extends Producto {
	private String codigoOrganismo;

	public ProductoRefrigerado(String fechaCaducidad, String numeroLote, String codigoOrganismo) {
		super(fechaCaducidad, numeroLote);
		this.codigoOrganismo = codigoOrganismo;
	}
	// Método getter para Codigo Organismo
	public String getCodigoOrganismo() {
		return codigoOrganismo;
	}

	public void setCodigoOrganismo(String codigoOrganismo) {
		this.codigoOrganismo = codigoOrganismo;
	}
	// Método toString
	@Override
	public String toString() {
		return super.toString() + "\nCódigo De Organismo= ~" + codigoOrganismo;
	}
}
