package alimentacion;

public class ProductoCongelado extends Producto {
	private double temperaturaCongelacionRecomendada;

	public ProductoCongelado(String fechaCaducidad, String numeroLote, double temperaturaCongelacionRecomendada) {
		super(fechaCaducidad, numeroLote);
		this.temperaturaCongelacionRecomendada = temperaturaCongelacionRecomendada;
	}

	// Método getter para temperaturaCongelacionRecomendada
	public double getTemperaturaCongelacionRecomendada() {
		return temperaturaCongelacionRecomendada;
	}

	// Método setter para temperaturaCongelacionRecomendada
	public void setTemperaturaCongelacionRecomendada(double temperaturaCongelacionRecomendada) {
		this.temperaturaCongelacionRecomendada = temperaturaCongelacionRecomendada;
	}

	// Método toString
	@Override
	public String toString() {
		return super.toString() + "\nTemperatura De Congelación Recomendada = " + temperaturaCongelacionRecomendada;
	}
}
