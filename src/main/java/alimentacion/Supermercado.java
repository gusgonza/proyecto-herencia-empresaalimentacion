package alimentacion;

public class Supermercado {
	public static void main(String[] args) {
		ProductoFresco fresco = new ProductoFresco("01/01/2025", "001", "09/01/2024", "España");
		ProductoRefrigerado refrigerado = new ProductoRefrigerado("01/06/2025", "002", "FDA12345");
		ProductoCongelado congelado = new ProductoCongelado("01/12/2025", "003", -18);

		// Mostrar información del Producto Fresco
		System.out.println("Información del Producto Fresco:");
		System.out.println(fresco.toString());
		System.out.println();

		// Mostrar información del Producto Refrigerado
		System.out.println("Información del Producto Refrigerado:");
		System.out.println(refrigerado.toString());
		System.out.println();

		// Mostrar información del Producto Congelado
		System.out.println("Información del Producto Congelado:");
		System.out.println(congelado.toString());
		System.out.println();
	}
}
