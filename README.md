# Herencia en Empresa de Alimentación

## Descripción

Este proyecto es una implementación de un sistema de gestión para un supermercado, donde se manejan diferentes tipos de productos alimenticios. Se utiliza el concepto de herencia para organizar los productos en clases específicas. 

## Clases Principales

- **Producto:** Clase base que representa un producto alimenticio con información común como fecha de caducidad y número de lote.
- **ProductoRefrigerado:** Clase que hereda de Producto y representa productos que requieren refrigeración, incluyendo el código del organismo de supervisión alimentaria.
- **ProductoCongelado:** Clase que hereda de Producto y representa productos que necesitan ser congelados, incluyendo la temperatura de congelación recomendada.
- **ProductoFresco:** Clase que hereda de Producto y representa productos frescos que no necesitan refrigeración especial, incluyendo la fecha de envasado y el país de origen.
- **Supermercado:** Clase principal donde se crean instancias de cada tipo de producto y se muestran los detalles de cada uno de ellos.


## Estructura del Proyecto
- **src/main/java/empresaAlimentacion/:** Contiene las implementaciones de las clases en Java.
- **build/:** Directorio que almacena los archivos compilados.

[![Estado de construcción](https://img.shields.io/static/v1?label=Estado%20de%20Construcción&message=Finalizado&color=success)](https://gitlab.com/gusgonza/proyecto-herencia-EmpresaAlimentacion)
