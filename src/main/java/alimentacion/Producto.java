package alimentacion;

public class Producto {
	protected String fechaCaducidad;
	protected String numeroLote;

	public Producto(String fechaActual, String numeroLote) {
		this.fechaCaducidad = fechaActual;
		this.numeroLote = numeroLote;
	}

	// Getter para la fecha de caducidad
	public String getFechaCaducidad() {
		return fechaCaducidad;
	}

	// Setter para la fecha de caducidad
	public void setFechaCaducidad(String fechaCaducidad) {
		this.fechaCaducidad = fechaCaducidad;
	}

	// Getter para el número de lote
	public String getNumeroLote() {
		return numeroLote;
	}

	// Setter para el número de lote
	public void setNumeroLote(String numeroLote) {
		this.numeroLote = numeroLote;
	}

	// Método toString
	@Override
	public String toString() {
		return "Fecha De Caducidad-> " + fechaCaducidad + "\nNúmero de Lote-> #" + numeroLote;
	}
}
