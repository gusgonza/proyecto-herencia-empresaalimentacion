package alimentacion;

public class ProductoFresco extends Producto {
	private String fechaEnvasado;
	private String paisOrigen;

	public ProductoFresco(String fechaCaducidad, String numeroLote, String fechaEnvasado, String paisOrigen) {
		super(fechaCaducidad, numeroLote);
		this.fechaEnvasado = fechaEnvasado;
		this.paisOrigen = paisOrigen;
	}

	// Métodos getter y setter para fechaEnvasado
	public String getFechaEnvasado() {
		return fechaEnvasado;
	}

	public void setFechaEnvasado(String fechaEnvasado) {
		this.fechaEnvasado = fechaEnvasado;
	}

	// Métodos getter y setter para paisOrigen
	public String getPaisOrigen() {
		return paisOrigen;
	}

	public void setPaisOrigen(String paisOrigen) {
		this.paisOrigen = paisOrigen;
	}

	// Método toString
	@Override
	public String toString() {
		return super.toString() + "\nFecha de Envasado= " + fechaEnvasado + "\nPaís Origen= " + paisOrigen;
	}
}
